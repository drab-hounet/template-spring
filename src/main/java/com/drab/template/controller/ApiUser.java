package com.drab.template.controller;

import com.drab.template.DAO.IAuthDAO;
import com.drab.template.DAO.IUserDAO;
import com.drab.template.model.Auth;
import com.drab.template.model.ResponseApi;
import com.drab.template.model.User;
import com.drab.template.utils.AuthUtils;
import com.drab.template.utils.LogMessageEnum;
import com.drab.template.utils.StatusEnum;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ApiUser {

    @Value("${token.security}")
    private String tokenSecurity;

    @Value("${token.validity}")
    private int tokenValidity;

    private IUserDAO userDAO;
    private IAuthDAO authDAO;
    private String logMessage;
    private ResponseEntity<ResponseApi> responseEntity;

    @Autowired
    public ApiUser(IUserDAO userDAO, IAuthDAO authDAO) {
        this.userDAO = userDAO;
        this.authDAO = authDAO;
    }

    @ApiOperation(value = "Récupère tous les utilisateurs")
    @GetMapping(value = "/api/users")
    public ResponseEntity<ResponseApi> getUsers(@RequestHeader(name = "Authorization") String token) {
        ResponseApi responseApi = new ResponseApi(StatusEnum.Error);
        responseEntity = new ResponseEntity<>(responseApi, HttpStatus.BAD_REQUEST);
        if (isAuthValid(token)) {
            List<User> users = userDAO.findAll();
            if (users != null) {
                responseApi.getStateApi().setStatus(StatusEnum.Success);
                for (User user : users) {
                    responseApi.getResponse().add(user);
                }
            }
            responseEntity = new ResponseEntity<>(responseApi, HttpStatus.OK);
        } else {
            responseApi.getStateApi().setLog(logMessage);
            responseEntity = new ResponseEntity<>(responseApi, HttpStatus.FORBIDDEN);
        }
        return responseEntity;
    }

    //Récupérer un user par son Id
    @ApiOperation(value = "Récupère un utilisateur grâce à son ID")
    @GetMapping(value = "/api/users/{id}")
    public ResponseEntity<ResponseApi> getUser(@PathVariable int id, @RequestHeader(name = "Authorization") String token) {
        ResponseApi responseApi = new ResponseApi(StatusEnum.Error);
        responseEntity = new ResponseEntity<>(responseApi, HttpStatus.BAD_REQUEST);
        if (isAuthValid(token)) {
            User user = userDAO.findById(id);
            if (user != null) {
                responseApi.getStateApi().setStatus(StatusEnum.Success);
                responseApi.getResponse().add(user);
            } else {
                responseApi.getStateApi().setLog("Aucun utilisateur avec l'id : " + id);
            }
            responseEntity = new ResponseEntity<>(responseApi, HttpStatus.OK);
        } else {
            responseApi.getStateApi().setLog(logMessage);
            responseEntity = new ResponseEntity<>(responseApi, HttpStatus.FORBIDDEN);
        }
        return responseEntity;
    }

    @ApiOperation(value = "Ajoute un utilisateur")
    @PostMapping(value = "/api/users")
    public ResponseEntity createUser(@RequestBody User userToCreate, @RequestHeader(name = "Authorization") String token) {
        ResponseApi responseApi = new ResponseApi(StatusEnum.Error);
        responseEntity = new ResponseEntity<>(responseApi, HttpStatus.BAD_REQUEST);
        if (isAuthValid(token)) {
            if (saveUser(userToCreate, responseApi) != null) {
                responseEntity = new ResponseEntity<>(responseApi, HttpStatus.CREATED);
            } else {
                responseEntity = new ResponseEntity<>(responseApi, HttpStatus.FORBIDDEN);
            }
        } else {
            responseApi.getStateApi().setLog(logMessage);
            responseEntity = new ResponseEntity<>(responseApi, HttpStatus.FORBIDDEN);
        }
        return responseEntity;
    }

    @ApiOperation(value = "Met à jour un utilisateur")
    @PutMapping(value = "/api/users")
    public ResponseEntity updateUser(@RequestBody User user, @RequestHeader(name = "Authorization") String token) {
        ResponseApi responseApi = new ResponseApi(StatusEnum.Error);
        responseEntity = new ResponseEntity<>(responseApi, HttpStatus.BAD_REQUEST);
        if (isAuthValid(token)) {
            User userToUpdate = userDAO.findById(user.getId());
            if (userToUpdate != null) {
                userToUpdate.setName(user.getName());
                userToUpdate.setMail(user.getMail());
                userToUpdate.setPicture(user.getPicture());
                if (saveUser(userToUpdate, responseApi) != null) {
                    responseEntity = new ResponseEntity<>(responseApi, HttpStatus.CREATED);
                } else {
                    responseEntity = new ResponseEntity<>(responseApi, HttpStatus.FORBIDDEN);
                }
            }
        } else {
            responseApi.getStateApi().setLog(logMessage);
            responseEntity = new ResponseEntity<>(responseApi, HttpStatus.FORBIDDEN);
        }
        return responseEntity;
    }

    @ApiOperation(value = "Efface un utilisateur")
    @DeleteMapping(value = "/api/users/{id}")
    public ResponseEntity deleteUser(@PathVariable int id, @RequestHeader(name = "Authorization") String token) {
        ResponseApi responseApi = new ResponseApi(StatusEnum.Error);
        responseEntity = new ResponseEntity<>(responseApi, HttpStatus.BAD_REQUEST);
        if (isAuthValid(token)) {
            User userToDelete = userDAO.findById(id);
            if (userToDelete != null) {
                responseApi.getStateApi().setStatus(StatusEnum.Success);
                userDAO.delete(userToDelete);
                responseEntity = new ResponseEntity<>(responseApi, HttpStatus.OK);
            }
        } else {
            responseApi.getStateApi().setLog(logMessage);
            responseEntity = new ResponseEntity<>(responseApi, HttpStatus.FORBIDDEN);
        }
        return responseEntity;
    }

    /**
     * Ajoute ou met à jour l'utilisateur
     *
     * @param userToUpdate utilisateur à mettre à jour
     * @param responseApi  response
     */
    private User saveUser(User userToUpdate, ResponseApi responseApi) {
        try {
            User userUpdated = userDAO.save(userToUpdate);
            if (userUpdated != null) {
                responseApi.getStateApi().setStatus(StatusEnum.Success);
                responseApi.getResponse().add(userUpdated);
            }
        } catch (Exception e) {
            responseApi.getStateApi().setLog(e.getMessage());
            return null;
        }
        return userToUpdate;
    }

    private boolean isAuthValid(String token) {
        String nameUser = AuthUtils.getNameUserFromToken(token, tokenSecurity);
        if (nameUser != null) {
            // Recherche de l'utilisateur
            List<User> users = userDAO.findByName(nameUser);
            if (users != null && users.size() == 1) {
                User userAuth = users.get(0);
                // Recherche de l'authentification
                List<Auth> listAuth = authDAO.findByUser(userAuth);
                if (listAuth != null && listAuth.size() == 1) {
                    Auth authUser = listAuth.get(0);
                    // Vérification que le token est bien le même qu'en bdd
                    if (authUser.getToken().equals(token)) {
                        if (AuthUtils.isTokenValid(authUser.getToken(), tokenSecurity)) {
                            return true;
                        } else {
                            logMessage = LogMessageEnum.ErrorTimeSessionAuth.toString();
                            return false;
                        }
                    }
                }
            }
        }
        logMessage = LogMessageEnum.ErrorAuth.toString();
        return false;
    }
}
