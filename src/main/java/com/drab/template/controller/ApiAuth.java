package com.drab.template.controller;

import com.drab.template.DAO.IAuthDAO;
import com.drab.template.DAO.IUserDAO;
import com.drab.template.model.Auth;
import com.drab.template.model.ResponseApi;
import com.drab.template.model.User;
import com.drab.template.utils.AuthUtils;
import com.drab.template.utils.LogMessageEnum;
import com.drab.template.utils.StatusEnum;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ApiAuth {

    private IUserDAO userDAO;
    private IAuthDAO authDAO;
    private ResponseEntity<ResponseApi> responseEntity;

    @Value("${token.security}")
    private String tokenSecurity;

    @Value("${token.validity}")
    private int tokenValidity;

    @Autowired
    public ApiAuth(IUserDAO userDAO, IAuthDAO authDAO) {
        this.userDAO = userDAO;
        this.authDAO = authDAO;
    }

    @ApiOperation(value = "Authentification d'utilisateur")
    @PostMapping(value = "/api/auth")
    public ResponseEntity<ResponseApi> authUser(@RequestBody User user) {
        ResponseApi responseApi = new ResponseApi(StatusEnum.Error);
        responseEntity = new ResponseEntity<>(responseApi, HttpStatus.BAD_REQUEST);
        List<User> usersAuth = userDAO.findByNameAndPassword(user.getName(), user.getPassword());
        // Si un utilsateur est valide ou on met à jour crée une authentification
        if (usersAuth != null && usersAuth.size() == 1) {
            User userAuth = usersAuth.get(0);
            String token = AuthUtils.createJWT(userAuth, tokenValidity, tokenSecurity);
            List<Auth> listAuth = authDAO.findByUser(userAuth);
            Auth newAuth = new Auth();
            if (listAuth != null && listAuth.size() == 1) {
                newAuth = listAuth.get(0);
            }
            newAuth.setUser(userAuth);
            newAuth.setToken(token);
            Auth authDB = authDAO.save(newAuth);
            responseApi.getStateApi().setStatus(StatusEnum.Success);
            responseApi.getResponse().add(authDB);
            responseEntity = new ResponseEntity<>(responseApi, HttpStatus.OK);
        } else {
            responseApi.getStateApi().setLog(LogMessageEnum.ErrorIdentificationAuth.toString());
            responseEntity = new ResponseEntity<>(responseApi, HttpStatus.FORBIDDEN);
        }

        return responseEntity;
    }
}
