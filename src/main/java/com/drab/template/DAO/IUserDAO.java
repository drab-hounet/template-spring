package com.drab.template.DAO;

import com.drab.template.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IUserDAO extends JpaRepository<User, Integer> {
    List<User> findAll();
    User findById(int id);
    List<User> findByNameAndPassword(String name, String password);
    List<User> findByName(String name);
}
