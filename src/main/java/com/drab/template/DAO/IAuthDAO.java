package com.drab.template.DAO;

import com.drab.template.model.Auth;
import com.drab.template.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IAuthDAO extends JpaRepository<Auth, Integer> {
    List<Auth> findByUser(User user);
}
