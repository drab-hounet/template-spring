package com.drab.template.model;

import com.drab.template.utils.StatusEnum;

import java.util.ArrayList;
import java.util.List;

public class ResponseApi {

    public ResponseApi(StatusEnum status) {
        this.stateApi = new StateApi();
        this.stateApi.setStatus(status);
        this.response =  new ArrayList<>();
    }

    private StateApi stateApi;
    private List<DataDBBase> response;

    public StateApi getStateApi() {
        return stateApi;
    }

    public void setStateApi(StateApi stateApi) {
        this.stateApi = stateApi;
    }

    public List<DataDBBase> getResponse() {
        return response;
    }

    public void setResponse(List<DataDBBase> response) {
        this.response = response;
    }
}
