package com.drab.template.model;

import com.drab.template.utils.StatusEnum;

public class StateApi {
    private StatusEnum status;
    private String log;

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }
}
