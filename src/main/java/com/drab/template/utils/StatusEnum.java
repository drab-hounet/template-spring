package com.drab.template.utils;

public enum StatusEnum {
    Success("success"),
    Error("error");

    public String name;

    StatusEnum(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }

}
