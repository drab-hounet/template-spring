package com.drab.template.utils;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;

import com.drab.template.model.User;
import io.jsonwebtoken.*;

import java.util.Date;

public class AuthUtils {

    public static String createJWT(User user, long ttlMillis, String tokenSecurity) {
        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(tokenSecurity);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId(String.valueOf(user.getId()))
                .setIssuedAt(now)
                .setSubject(SubjectEnum.Admin.toString())
                .setIssuer(user.getName())
                .signWith(signatureAlgorithm, signingKey);

        //if it has been specified, let's add the expiration
        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    /**
     * Récupération du nom de l'utilisateur
     *
     * @param jwt token
     * @param tokenSecurity permet de lire le token
     * @return le nom de l'utilisateur
     */
    public static String getNameUserFromToken(String jwt, String tokenSecurity) {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(DatatypeConverter.parseBase64Binary(tokenSecurity))
                    .parseClaimsJws(jwt).getBody();
            return claims.getIssuer();
        } catch (Exception e) {
            return null;
        }

    }

    public static boolean isTokenValid(String jwt, String tokenSecurity) {
        //This line will throw an exception if it is not a signed JWS (as expected)
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(tokenSecurity))
                .parseClaimsJws(jwt).getBody();
        return claims.getExpiration().getTime() > System.currentTimeMillis();
    }
}
