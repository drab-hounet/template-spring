package com.drab.template.utils;

public enum LogMessageEnum {
    ErrorAuth("authentification error"),
    ErrorIdentificationAuth("user or password not valid"),
    ErrorTimeSessionAuth("authentification time session error");

    public String message;

    LogMessageEnum(String message) {
        this.message = message;
    }

    public String toString() {
        return message;
    }
}
