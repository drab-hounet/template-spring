package com.drab.template.utils;

public enum SubjectEnum {
    Read("read"),
    Admin("admin");

    public String type;
    SubjectEnum(String name) {
        this.type = name;
    }

    public String toString() {
        return type;
    }
    }
